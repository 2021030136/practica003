package Controlador;

import Vista.jlgBomba;
import Modelo.Bomba;
import Modelo.Gasolina;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class controlador implements ActionListener {

    private Bomba bom;
    private jlgBomba vista;
    private int auto;

    public controlador(Bomba bom, jlgBomba vista) {
        this.bom = bom;
        this.vista = vista;

        //si inicializan los botones del programa
        vista.btniniBomba.addActionListener(this);
        vista.btnRegistar.addActionListener(this);
        vista.cbxTipo.addActionListener(this);
        vista.btnCerrar.addActionListener(this);

    }
    //inicialisamos la vista

    private void iniciarVista() {
        vista.setTitle("Bomba");
        vista.setSize(850, 500);
        vista.setVisible(true);
    }
    //btn nuevo

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.cbxTipo) {
            switch (vista.cbxTipo.getSelectedIndex()) {
                case 0:
                    vista.txtpreventa.setText("24");
                    break;
                case 1:
                    vista.txtpreventa.setText("29");
                    break;
                case 2:
                    vista.txtpreventa.setText("30");
                    break;
            }

        }
        if(e.getSource() == vista.btnCerrar){
            int respuesta = JOptionPane.showConfirmDialog(vista, "¿Quieres salir",
                    "Salir", JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_NO_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        }
        if (e.getSource() == vista.cbxTipo) {
            vista.btniniBomba.setEnabled(true);
        }
        if (e.getSource() == vista.btniniBomba) {
            //bom.getGasolina().setTipo(vista.cbxTipo.getSelectedIndex());
            try {
                vista.txtcant.setEditable(true);
                vista.btnRegistar.setEnabled(true);
                vista.TxtNumbom.setEnabled(true);
                vista.txtconven.setEnabled(false);
                vista.txtcant.setEnabled(true);
                vista.cbxTipo.setEnabled(false);
                bom.iniciarBomba(Integer.parseInt(vista.TxtNumbom.getText()),
                        new Gasolina(vista.cbxTipo.getSelectedIndex(), vista.cbxTipo.getSelectedItem().toString(), Float.parseFloat(vista.txtpreventa.getText())), vista.sldCapbomba.getValue(), 0);
                vista.sldCapbomba.setValue((int) bom.getCapBomba());

            } catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error" + ex.getMessage());
            }
            }
        
            //btn registrar
            if (e.getSource() == vista.btnRegistar) {
                try {
                    if (bom.inventarioGasolina() >= Float.parseFloat(vista.txtcant.getText())) {
                        float gas = bom.vendarGasolina(Float.parseFloat(vista.txtcant.getText()));
                        vista.JdlCosto.setText(String.valueOf(gas));
                        vista.jlatotalVentas.setText(String.valueOf(bom.ventasTotales()));
                        vista.sldCapbomba.setValue((int) bom.inventarioGasolina());
                        auto++;
                        vista.txtconven.setText(String.valueOf(auto));
                    } else {
                        JOptionPane.showMessageDialog(vista, "Suministro Insuficiente");
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(vista, "Surgio el siguiente error" + ex.getMessage());
                } catch (Exception ex2) {
                    JOptionPane.showMessageDialog(vista, "Surgio el siguiente error" + ex2.getMessage());
                }
            }

            
            
            
            
        }
    

    public static void main(String[] args) {
        Bomba bom = new Bomba();
        jlgBomba vista = new jlgBomba(new JFrame(), true);

        controlador contra = new controlador(bom, vista);
        contra.iniciarVista();
}
}

