/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author bmfil
 */
public class Gasolina {
    private int idGasolina;
    private String tipo;
    private Float precio;

    public Gasolina(int idGasolina, String tipo, Float precio) {
        this.idGasolina = idGasolina;
        this.tipo = tipo;
        this.precio = precio;
    }
    public Gasolina(){
        this.idGasolina=0;
        this.precio=0.0f;
        this.tipo= "";
    }
      public Gasolina(Gasolina gas) {
        this.idGasolina = gas.idGasolina;
        this.tipo = gas.tipo;
        this.precio = gas.precio;
    } 
    public int getIdGasolina() {
        return idGasolina;
    }

    public void setIdGasolina(int idGasolina) {
        this.idGasolina = idGasolina;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }
    
}
