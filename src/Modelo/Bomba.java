
package Modelo;


public class Bomba {
    private int numdeBomba;
    private float capBomba;
    private float acuLitros;
    private Gasolina gasolina;

    public Bomba(int numdeBomba, float capBomba, float acuLitros, Gasolina gasolina) {
        this.numdeBomba = numdeBomba;
        this.capBomba = capBomba;
        this.acuLitros = acuLitros;
        this.gasolina = gasolina;
    }
    public Bomba(){
        this.numdeBomba=0;
        this.capBomba=0.0f;
        this.acuLitros=0.0f;
        this.gasolina=null;
           
    }
      public Bomba(Bomba bom) {
        this.numdeBomba = bom.numdeBomba;
        this.capBomba = bom.capBomba;
        this.acuLitros = bom.acuLitros;
        this.gasolina = bom.gasolina;
    }
    
    
    public int getNumdeBomba() {
        return numdeBomba;
    }

    public void setNumdeBomba(int numdeBomba) {
        this.numdeBomba = numdeBomba;
    }

    public float getCapBomba() {
        return capBomba;
    }

    public void setCapBomba(float capBomba) {
        this.capBomba = capBomba;
    }

    public float getAcuLitros() {
        return acuLitros;
    }

    public void setAcuLitros(float acuLitros) {
        this.acuLitros = acuLitros;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }
    
    public void iniciarBomba(int numbomba, Gasolina gasolina,float capBomba, float aculitros){
        this.numdeBomba = numbomba;
        this.acuLitros = 0;
        this.capBomba = 200;
        this.gasolina = gasolina;
    }
    
    public float inventarioGasolina(){
        return capBomba - acuLitros;
    }
    public float vendarGasolina(float acuLitros){
        if(inventarioGasolina()>= acuLitros ){
            this.acuLitros+= acuLitros;
        }
        return acuLitros * this.gasolina.getPrecio();
    }
    public float ventasTotales(){
        return acuLitros * this.gasolina.getPrecio();
    }
}
